import 'package:flutter/material.dart';
import 'package:simple_circular_progress_bar/simple_circular_progress_bar.dart';
import 'package:stacked/stacked.dart';
import 'package:todo_app/app/app.router.dart';
import 'package:todo_app/core/enums/task_type.dart';

import 'home_viewmodel.dart';

class HomeView extends StackedView<HomeViewModel> {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget builder(
    BuildContext context,
    HomeViewModel viewModel,
    Widget? child,
  ) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10),
          child: ListView(
            children: [
              Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.white, border: Border.all(width: 1, color: Colors.black12)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: SimpleCircularProgressBar(
                        mergeMode: true,
                        valueNotifier: viewModel.completePercentage,
                        onGetText: (double value) {
                          return Text('${value.toInt()}%');
                        },
                      ),
                    ),
                    const SizedBox(width: 30),
                    Expanded(
                      child: Container(
                        alignment: Alignment.centerRight,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            const Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "Total Tasks",
                                  style: TextStyle(fontSize: 18),
                                ),
                              ],
                            ),
                            const SizedBox(height: 16),
                            Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    height: 40,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), border: Border.all(width: 1, color: Colors.black12)),
                                    child: Text(
                                      "${viewModel.countTask}",
                                      style: const TextStyle(fontSize: 16, color: Colors.greenAccent),
                                    ),
                                  ),
                                ),
                                const SizedBox(width: 10),
                                Expanded(
                                  child: Container(
                                    height: 40,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), border: Border.all(width: 1, color: Colors.black12)),
                                    child: Text(
                                      viewModel.countTaskCompleted.toString(),
                                      style: const TextStyle(fontSize: 16, color: Colors.redAccent),
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                padding: EdgeInsets.symmetric(vertical: 10),
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.white, border: Border.all(width: 1, color: Colors.black12)),
                child: TextField(
                  controller: viewModel.txtSearch,
                  textAlign: TextAlign.start,
                  keyboardType: TextInputType.text,
                  onChanged: (value) {
                    viewModel.getAllTasks();
                  },
                  decoration: const InputDecoration(
                    hintText: 'Search by Title and Description',
                    hintStyle: TextStyle(fontSize: 16, color: Colors.black38),
                    border: InputBorder.none,
                    filled: false,
                    contentPadding: EdgeInsets.all(16),
                  ),
                ),
              ),
              ...List.generate(viewModel.tasks.length, (index) {
                var task = viewModel.tasks.elementAt(index);
                return Dismissible(
                  key: UniqueKey(),
                  direction: DismissDirection.startToEnd,
                  onDismissed: (direction) {
                    // Then show a snackbar.
                    viewModel.deleteTask(task.id ?? 0, index);
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('${task.title} was removed')));
                  },
                  child: Container(
                    margin: const EdgeInsets.only(top: 5),
                    padding: const EdgeInsets.all(6),
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.white, border: Border.all(width: 1, color: Colors.black12)),
                    child: ListTile(
                      leading: Checkbox(
                        value: task.isCompleted,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(6),
                        ),
                        side: BorderSide(width: 1, color: Theme.of(context).unselectedWidgetColor),
                        onChanged: (bool? value) {
                          viewModel.updateStatusTask(task.id ?? 0, task.isCompleted ? false : true);
                        },
                      ),
                      contentPadding: EdgeInsets.zero,
                      title: Text(task.title),
                      subtitle: Text(
                        task.dueDate,
                        style: const TextStyle(fontSize: 12, color: Colors.black38),
                      ),
                      trailing: PopupMenuButton<String>(
                        icon: const Icon(Icons.more_vert), // Setting the icon
                        onSelected: (value) async {
                          // Perform actions based on the selected value
                          if (value == 'edit') {
                            viewModel.navigationService.navigateToCreateTaskView(type: TaskType.edit, taskId: task.id).then((value) {
                              viewModel.getInstance();
                            });
                          } else if (value == 'delete') {
                            await viewModel.deleteTask(task.id ?? 0, index);
                          }
                        },
                        itemBuilder: (BuildContext context) {
                          return <PopupMenuEntry<String>>[
                            const PopupMenuItem<String>(
                              value: 'edit',
                              child: Text('Edit'),
                            ),
                            const PopupMenuItem<String>(
                              value: 'delete',
                              child: Text('Delete'),
                            ),
                          ];
                        },
                      ),
                    ),
                  ),
                );
              }),
              if (viewModel.tasks.isEmpty)
                Padding(
                  padding: const EdgeInsets.only(top: 70),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Icon(Icons.payments_outlined),
                      ),
                      const Text(
                        'No result!',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0xFF545454),
                          fontSize: 16,
                          fontFamily: 'Poppins',
                          fontWeight: FontWeight.bold,
                          height: 0,
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'To create this new task with this ${viewModel.txtSearch.text}?',
                            textAlign: TextAlign.center,
                            style: const TextStyle(
                              color: Color(0xFF545454),
                              fontSize: 14,
                              fontFamily: 'Poppins',
                              fontWeight: FontWeight.bold,
                              height: 0,
                            ),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          GestureDetector(
                            child: const Icon(
                              Icons.add,
                              color: Colors.green,
                            ),
                            onTap: () {
                              viewModel.navigationService.navigateToCreateTaskView(type: TaskType.create, taskTitle: viewModel.txtSearch.text).then((value) {
                                viewModel.getInstance();
                              });
                            },
                          )
                        ],
                      )
                    ],
                  ),
                )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton.small(
          onPressed: () {
            viewModel.navigationService.navigateToCreateTaskView(type: TaskType.create, taskTitle: viewModel.txtSearch.text).then((value) {
              viewModel.getInstance();
            });
          },
          backgroundColor: Colors.green,
          child: const Icon(
            Icons.add,
          )),
    );
  }

  @override
  HomeViewModel viewModelBuilder(
    BuildContext context,
  ) =>
      HomeViewModel();
  @override
  void onViewModelReady(HomeViewModel viewModel) async {
    await viewModel.getInstance();
    super.onViewModelReady(viewModel);
  }
}
