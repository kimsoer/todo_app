import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:todo_app/app/app.locator.dart';
import 'package:todo_app/core/db/database_helper.dart';
import 'package:todo_app/core/model/task_model.dart';

class HomeViewModel extends BaseViewModel {
  final _dialogService = locator<DialogService>();
  final _bottomSheetService = locator<BottomSheetService>();
  final navigationService = locator<NavigationService>();
  var taskDao = DatabaseHelper.instance.taskDao;
  List<TaskModel> tasks = [];
  TextEditingController txtSearch = TextEditingController();

  int countTask = 0;
  int countTaskCompleted = 0;
  final ValueNotifier<double> completePercentage = ValueNotifier<double>(0.0);

  getInstance() async {
    clearErrors();
    setBusy(false);

    await getCountAllTasks();
    await getCountCompletedAllTasks();
    await getTotalCompletedPercentage();
    await getAllTasks();
    setInitialised(true);
    notifyListeners();
  }

  // fetch all task by asc date
  getAllTasks() async {
    // try {
    setBusy(true);
    tasks = await taskDao.getTasks(searchQuery: txtSearch.text);
    if (tasks.isNotEmpty) {
      print("task=> ${tasks.last.toMap()}");
    }
    // } catch (error) {
    //   print("error=> ${error.toString()}");
    // } finally {
    setBusy(false);
    notifyListeners();
    // }
  }

  // Can remove by swipe start to end or popup menu
  deleteTask(int id, int index) async {
    try {
      setBusy(true);
      await taskDao.deleteTask(id);
      tasks.removeAt(index);
      getInstance();
    } catch (error) {
      print("error=> ${error.toString()}");
    } finally {
      setBusy(false);
      notifyListeners();
    }
  }

  getCountAllTasks() async {
    try {
      setBusy(true);
      countTask = await taskDao.countAllTasks();
    } catch (error) {
      print("error ${error.toString()}");
    } finally {
      setBusy(false);
      notifyListeners();
    }
  }

  getCountCompletedAllTasks() async {
    try {
      setBusy(true);
      countTaskCompleted = await taskDao.countAllCompletedTasks();
    } catch (error) {
      print("error ${error.toString()}");
    } finally {
      setBusy(false);
      notifyListeners();
    }
  }

  updateStatusTask(int taskId, bool isCompleted) async {
    try {
      setBusy(true);
      var result = await taskDao.updateTaskCompletion(taskId, isCompleted);
      getInstance();
    } catch (error) {
      print("error ${error.toString()}");
    } finally {
      setBusy(false);
      notifyListeners();
    }
  }

  getTotalCompletedPercentage() async {
    try {
      setBusy(true);
      completePercentage.value = await taskDao.calculatePercentageCompleted();
    } catch (error) {
      print("error ${error.toString()}");
    } finally {
      setBusy(false);
      notifyListeners();
    }
  }
}
