import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:todo_app/app/app.dialogs.dart';
import 'package:todo_app/app/app.locator.dart';
import 'package:todo_app/core/db/database_helper.dart';
import 'package:todo_app/core/model/category_model.dart';
import 'package:todo_app/core/model/task_model.dart';

class CreateTaskViewModel extends BaseViewModel {
  int selectedItemIndex = -1; // Initialize with no item selected

  final _dialogService = locator<DialogService>();
  List<CategoryModel> categoryItems = [];
  var categoryDao = DatabaseHelper.instance.categoryDao;
  var taskDao = DatabaseHelper.instance.taskDao;
  String? dateFormat;
  DateTime initValue = DateTime.now();
  TextEditingController txtTitle = TextEditingController();
  TextEditingController txtDescription = TextEditingController();
  CategoryModel categoryModelSelected = CategoryModel(name: '');
  bool isEnableSaveButton = false;
  getInstance(int id) async {
    clearErrors();
    setBusy(false);

    await getCategoryList();
    await getAllTasks();
    await getTaskById(id);
    setInitialised(true);
    notifyListeners();
  }

  void handleSelection(int index) {
    selectedItemIndex = index;
    categoryModelSelected = categoryItems.elementAt(index);
    setEnableButton();

    notifyListeners();
  }

  void showDialogToCreateCategory() {
    var result = _dialogService.showCustomDialog(
      variant: DialogType.addCategory,
      title: 'Stacked Rocks! category',
      description: 'Give stacked',
    );
    result.then((value) async {
      print("object ${value?.confirmed}");
      if (value?.confirmed ?? false) {
        await getCategoryList();
      }
    });
  }

  Future<void> selectDate(BuildContext context) async {
    DateTime? picked = await showDatePicker(
      context: context,
      initialDate: initValue,
      firstDate: DateTime(2020),
      lastDate: DateTime(2025),
    );
    if (picked != null) {
      var pickedTime = await showTimePicker(
        context: context,
        initialTime: TimeOfDay.now(),
      );
      if (pickedTime != null) {
        picked = DateTime(
          picked.year,
          picked.month,
          picked.day,
          pickedTime.hour,
          pickedTime.minute,
        );
        // Do something with the selected date
        String formattedDateTime = DateFormat('d-MM-y, hh:mm a').format(picked);
        if (formattedDateTime.isNotEmpty) {
          dateFormat = formattedDateTime;
          setEnableButton();
        }
      }
    }
  }

  setEnableButton() {
    isEnableSaveButton = txtTitle.text.isNotEmpty && (dateFormat?.isNotEmpty ?? false) && categoryModelSelected.name.isNotEmpty;
    notifyListeners();
  }

  getCategoryList() async {
    try {
      setBusy(true);
      categoryItems = await categoryDao.getCategories();

      print("object ${categoryItems.toString()}");
    } catch (error) {
      print("error=> ${error.toString()}");
    } finally {
      setBusy(false);
      notifyListeners();
    }
  }

  createTask(BuildContext context) async {
    try {
      setBusy(true);

      TaskModel taskModel = TaskModel(
          categoryId: categoryModelSelected.id ?? -1,
          title: txtTitle.text,
          description: txtDescription.text,
          dueDate: dateFormat ?? DateFormat('d-MM-y, hh:mm a').format(DateTime.now()),
          isCompleted: false);
      await taskDao.insertTask(taskModel).then((value) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(
          '${txtTitle.text} create successfully!',
          style: TextStyle(color: Theme.of(context).primaryColor),
        )));
        // Navigator.pop(context);
        getAllTasks();
      });
    } catch (error) {
      if (error.toString().contains("already exists")) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            duration: Duration(seconds: 5),
            content: Text(
              error.toString().replaceFirst("Exception:", " "),
              style: const TextStyle(color: Colors.redAccent),
            ),
          ),
        );
      }
      print("object ${error.toString().replaceFirst("Exceptions: ", " ")}");
    } finally {
      setBusy(false);
      notifyListeners();
    }
  }

  updateTask(BuildContext context, int id) async {
    try {
      setBusy(true);

      TaskModel taskModel = TaskModel(
          id: id,
          categoryId: categoryItems.elementAt(selectedItemIndex).id ?? 0,
          title: txtTitle.text,
          description: txtDescription.text,
          dueDate: dateFormat ?? DateFormat('d-MM-y, hh:mm a').format(DateTime.now()),
          isCompleted: false);
      await taskDao.updateTask(taskModel).then((value) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('${taskModel.title} was updated successfully!')));
      });
      getAllTasks();
    } catch (error) {
      print("object ${error.toString()}");
    } finally {
      setBusy(false);
      notifyListeners();
    }
  }

  getAllTasks() async {
    try {
      setBusy(true);
      var response = await taskDao.getTasks();
      print("task=> ${response.last.toMap()}");
    } catch (error) {
      print("object ${error.toString()}");
    } finally {
      setBusy(false);
      notifyListeners();
    }
  }

  getTaskById(int id) async {
    try {
      setBusy(true);
      var response = await taskDao.getTaskById(id);
      txtDescription.text = response?.description ?? '';
      txtTitle.text = response?.title ?? '';
      dateFormat = response?.dueDate;
      DateFormat format = DateFormat("dd-MM-yyyy, hh:mm a");
      DateTime dateTime = format.parse(dateFormat!);

      initValue = dateTime;
      var index = categoryItems.lastIndexWhere((element) => element.id == response?.categoryId);
      print("index ${index}");
      handleSelection(index);
      return response;
    } catch (error) {
      print("object ${error.toString()}");
    } finally {
      setBusy(false);
      notifyListeners();
    }
  }
}
