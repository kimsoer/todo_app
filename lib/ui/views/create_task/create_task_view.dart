import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:todo_app/core/enums/task_type.dart';
import 'package:todo_app/ui/common/ui_helpers.dart';

import 'create_task_viewmodel.dart';

class CreateTaskView extends StackedView<CreateTaskViewModel> {
  const CreateTaskView({Key? key, this.type, this.taskId, this.taskTitle}) : super(key: key);
  final TaskType? type;
  final int? taskId;
  final String? taskTitle;

  @override
  Widget builder(
    BuildContext context,
    CreateTaskViewModel viewModel,
    Widget? child,
  ) {
    return Scaffold(
      appBar: AppBar(
        title: Text(" ${type == TaskType.create ? 'Adding' : 'Update'} Task"),
      ),
      body: ListView(
        padding: const EdgeInsets.all(6),
        children: [
          TextField(
            controller: viewModel.txtTitle,
            textAlign: TextAlign.start,
            keyboardType: TextInputType.text,
            onChanged: (value) {
              viewModel.setEnableButton();
            },
            decoration: InputDecoration(
              hintText: 'Task Title',
              hintStyle: TextStyle(fontSize: 16),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: BorderSide(
                  width: 0.5,
                  color: Colors.black12,
                  style: BorderStyle.none,
                ),
              ),
              filled: true,
              contentPadding: EdgeInsets.all(16),
            ),
          ),
          const SizedBox(height: 10),
          TextField(
            controller: viewModel.txtDescription,
            textAlign: TextAlign.start,
            keyboardType: TextInputType.text,
            maxLines: 10,
            decoration: InputDecoration(
              hintText: 'Task Description (optional)',
              hintStyle: const TextStyle(fontSize: 16),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8),
                borderSide: const BorderSide(
                  width: 0.5,
                  color: Colors.black12,
                  style: BorderStyle.none,
                ),
              ),
              filled: true,
              contentPadding: EdgeInsets.all(16),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 10, bottom: 10),
            decoration: BoxDecoration(color: Colors.green.shade50, borderRadius: BorderRadius.circular(10)),
            child: ListTile(
              onTap: () {
                viewModel.selectDate(context);
              },
              leading: Icon(Icons.calendar_month, color: Colors.green.shade900),
              title: Text(
                "Select Date in Calendar",
                style: TextStyle(fontSize: 16, color: Colors.green.shade900),
              ),
              trailing: Icon(Icons.arrow_forward_ios, color: Colors.green.shade900),
            ),
          ),
          ListTile(
            onTap: () {
              viewModel.showDialogToCreateCategory();
            },
            contentPadding: EdgeInsets.zero,
            title: const Text(
              "Choose Category (required)",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            trailing: const Icon(
              Icons.add,
              color: Colors.green,
            ),
          ),
          viewModel.categoryItems.isNotEmpty
              ? Wrap(
                  spacing: 10.0,
                  runSpacing: 10.0,
                  children: List.generate(
                    viewModel.categoryItems.length,
                    (index) => GestureDetector(
                      onTap: () {
                        viewModel.handleSelection(index);
                      },
                      child: Container(
                        padding: const EdgeInsets.all(12),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: viewModel.selectedItemIndex == index
                                ? Theme.of(context).primaryColor // Highlight selected item
                                : Colors.grey, // Default border color
                          ),
                          borderRadius: BorderRadius.circular(6),
                        ),
                        child: Text(
                          viewModel.categoryItems[index].name,
                          style: TextStyle(
                              color: viewModel.selectedItemIndex == index
                                  ? Theme.of(context).primaryColor // Highlight selected item
                                  : Colors.grey),
                        ),
                      ),
                    ),
                  ),
                )
              : const Text(
                  "Category is empty!, please add it",
                ),
          verticalSpaceLarge,
          GestureDetector(
            onTap: () {
              if (viewModel.isEnableSaveButton) {
                if (type == TaskType.create) {
                  viewModel.createTask(context);
                } else {
                  viewModel.updateTask(context, taskId ?? 0);
                }
              }
            },
            child: Container(
              height: 50,
              margin: const EdgeInsets.symmetric(horizontal: 10),
              width: double.infinity,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: viewModel.isEnableSaveButton ? Theme.of(context).primaryColor : Colors.grey,
                borderRadius: BorderRadius.circular(10),
              ),
              child: const Text(
                'Add it',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  CreateTaskViewModel viewModelBuilder(
    BuildContext context,
  ) =>
      CreateTaskViewModel();

  @override
  void onViewModelReady(CreateTaskViewModel viewModel) async {
    await viewModel.getInstance(taskId ?? 0);
    super.onViewModelReady(viewModel);
  }
}
