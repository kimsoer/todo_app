import 'package:flutter/material.dart';

class AppConfig {
  static const String appName = "Mini POS";

  static String baseApiUrl = "";

  static const supportedLocale = [
    Locale('en', 'US'),
    Locale('km', 'KH'),
    Locale('fr', 'FR'),
  ];
}
