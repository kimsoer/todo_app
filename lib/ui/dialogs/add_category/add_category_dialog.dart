import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:todo_app/ui/common/ui_helpers.dart';

import 'add_category_dialog_model.dart';

const double _graphicSize = 60;

class AddCategoryDialog extends StackedView<AddCategoryDialogModel> {
  final DialogRequest request;
  final Function(DialogResponse) completer;

  const AddCategoryDialog({
    Key? key,
    required this.request,
    required this.completer,
  }) : super(key: key);

  @override
  Widget builder(
    BuildContext context,
    AddCategoryDialogModel viewModel,
    Widget? child,
  ) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      backgroundColor: Colors.white,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Align(
              alignment: Alignment.topRight,
              child: GestureDetector(
                child: const Icon(Icons.close, color: Colors.redAccent),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ),
            const SizedBox(height: 10),
            TextField(
              controller: viewModel.txtCatName,
              textAlign: TextAlign.start,
              onChanged: (value) {
                viewModel.setEnableButton();
              },
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                hintText: 'Task Title',
                hintStyle: TextStyle(fontSize: 16),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: const BorderSide(
                    width: 0.5,
                    color: Colors.black12,
                    style: BorderStyle.none,
                  ),
                ),
                filled: true,
                contentPadding: EdgeInsets.all(16),
              ),
            ),
            const SizedBox(height: 10),
            TextField(
              controller: viewModel.txtCatDescription,
              textAlign: TextAlign.start,
              keyboardType: TextInputType.text,
              maxLines: 10,
              decoration: InputDecoration(
                hintText: 'Task Description (optional)',
                hintStyle: const TextStyle(fontSize: 16),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: const BorderSide(
                    width: 0.5,
                    color: Colors.black12,
                    style: BorderStyle.none,
                  ),
                ),
                filled: true,
                contentPadding: EdgeInsets.all(16),
              ),
            ),
            verticalSpaceMedium,
            GestureDetector(
              onTap: !viewModel.isEnableButton
                  ? null
                  : () {
                      completer(DialogResponse(confirmed: true));
                      viewModel.createCategory();
                    },
              child: Container(
                height: 50,
                width: double.infinity,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: !viewModel.isEnableButton ? Colors.grey : Colors.black,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: const Text(
                  'Add it',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  AddCategoryDialogModel viewModelBuilder(BuildContext context) => AddCategoryDialogModel();
}
