import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:todo_app/core/db/database_helper.dart';
import 'package:todo_app/core/model/category_model.dart';

class AddCategoryDialogModel extends BaseViewModel {
  //Local database init
  TextEditingController txtCatName = TextEditingController();
  TextEditingController txtCatDescription = TextEditingController();
  var categoryDao = DatabaseHelper.instance.categoryDao;

  bool isEnableButton = false;

  createCategory() async {
    try {
      setBusy(true);
      CategoryModel newCategory = CategoryModel(name: txtCatName.text, description: txtCatDescription.text);
      await categoryDao.insertCategory(newCategory);
      var response = await categoryDao.getCategories();
      print("respone=> ${response.first.toMap()}");
    } catch (error) {
      print("error=> ${error.toString()}");
    } finally {
      setBusy(false);
      notifyListeners();
    }
  }

  setEnableButton() {
    if (txtCatName.text.isNotEmpty) {
      isEnableButton = true;
    } else {
      isEnableButton = false;
    }
    notifyListeners();
  }
}
