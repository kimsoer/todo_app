class TaskModel {
  int? id;
  int categoryId;
  String title;
  String description;
  String dueDate;
  bool isCompleted;

  TaskModel({
    this.id,
    required this.categoryId,
    required this.title,
    required this.description,
    required this.dueDate,
    required this.isCompleted,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'categoryId': categoryId,
      'title': title,
      'description': description,
      'dueDate': dueDate,
      'isCompleted': isCompleted ? 1 : 0,
    };
  }

  factory TaskModel.fromMap(Map<String, dynamic> map) {
    return TaskModel(
      id: map['id'],
      title: map['title'],
      description: map['description'],
      dueDate: map['dueDate'],
      isCompleted: map['isCompleted'] == 1,
      categoryId: map['categoryId'],
    );
  }
}
