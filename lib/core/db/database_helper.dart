import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart';
import 'package:todo_app/core/dao/category_dao.dart';
import 'package:todo_app/core/dao/task_dao.dart';

class DatabaseHelper {
  static DatabaseHelper? _instance;
  static Database? _database;
  static const String taskTableName = 'task_tbl';
  static const String categoryTableName = 'categories_tbl';

  late TaskDao taskDao;
  late CategoryDao categoryDao;

  // Private constructor
  DatabaseHelper._();

  // Singleton instance retrieval
  static DatabaseHelper get instance {
    _instance ??= DatabaseHelper._();
    return _instance!;
  }

  Future<Database> get database async {
    if (_database != null) return _database!;

    _database = await initDatabase();
    taskDao = TaskDao(_database!);
    categoryDao = CategoryDao(_database!);
    return _database!;
  }

  Future<Database> initDatabase() async {
    String databasesPath = await getDatabasesPath();
    String pathToDatabase = path.join(databasesPath, 'todo_database.db');
    return await openDatabase(
      pathToDatabase,
      version: 1,
      onCreate: (Database db, int version) async {
        await db.execute('''
          CREATE TABLE $taskTableName (
            id INTEGER PRIMARY KEY,
            title TEXT,
            description TEXT,
            dueDate TEXT,
            isCompleted INTEGER,
            categoryId INTEGER,
            FOREIGN KEY (categoryId) REFERENCES $categoryTableName(id)
          )
        ''');

        await db.execute('''
          CREATE TABLE $categoryTableName (
            id INTEGER PRIMARY KEY,
            name TEXT,
            description TEXTds
          )
        ''');
      },
    );
  }
}
