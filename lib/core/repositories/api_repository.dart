import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

part 'api_repository.g.dart';

@RestApi(baseUrl: "AppConfig.baseApiUrl")
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;
}
