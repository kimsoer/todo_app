import 'package:sqflite/sqflite.dart';
import 'package:todo_app/core/model/category_model.dart';

class CategoryDao {
  late Database _db;
  static const String categoryTableName = 'categories_tbl';

  CategoryDao(this._db);

  Future<void> insertCategory(CategoryModel category) async {
    await _db.insert(categoryTableName, category.toMap());
  }

  Future<List<CategoryModel>> getCategories() async {
    final List<Map<String, dynamic>> maps = await _db.query(categoryTableName);
    return List.generate(maps.length, (i) {
      return CategoryModel.fromMap(maps[i]);
    });
  }

// Other CRUD operations for categories
}
