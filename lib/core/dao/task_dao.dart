import 'package:sqflite/sqflite.dart';
import 'package:todo_app/core/model/task_model.dart';

class TaskDao {
  late final Database _db;
  static const String tableName = 'task_tbl';

  TaskDao(this._db);

  Future<void> insertTask(TaskModel task) async {
    List<Map<String, dynamic>> existingTasks = await _db.query(
      tableName,
      where: 'title = ? AND description = ? AND dueDate = ? AND isCompleted = ? AND categoryId = ?',
      whereArgs: [
        task.title,
        task.description,
        task.dueDate,
        task.isCompleted ? 1 : 0,
        task.categoryId,
      ],
    );

    if (existingTasks.isEmpty) {
      await _db.insert(tableName, task.toMap());
    } else {
      throw Exception('Task with the same fields already exists');
    }
  }

  Future<void> editTask(int taskId, TaskModel updatedTask) async {
    await _db.update(
      tableName,
      updatedTask.toMap(),
      where: 'id = ?',
      whereArgs: [taskId],
    );
  }

  Future<List<TaskModel>> getTasks({String? searchQuery}) async {
    late List<Map<String, dynamic>> maps;

    if (searchQuery != null && searchQuery.isNotEmpty) {
      maps = await _db.query(
        tableName,
        where: 'title LIKE ? OR description LIKE ?',
        whereArgs: ['%$searchQuery%', '%$searchQuery%'],
        orderBy: 'dueDate ASC',
      );
    } else {
      maps = await _db.query(
        tableName,
        orderBy: 'dueDate ASC',
      );
    }

    return List.generate(maps.length, (i) {
      return TaskModel.fromMap(maps[i]);
    });
  }

  Future<TaskModel?> getTaskById(int id) async {
    List<Map<String, dynamic>> result = await _db.query(
      tableName,
      where: 'id = ?',
      whereArgs: [id],
    );

    if (result.isEmpty) {
      return null; // Return null if no task found with the given ID
    }
    return TaskModel.fromMap(result.first);
  }

  Future<void> updateTask(TaskModel task) async {
    await _db.update(
      tableName,
      task.toMap(),
      where: 'id = ?',
      whereArgs: [task.id],
    );
  }

  Future<void> deleteTask(int id) async {
    await _db.delete(
      tableName,
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  Future<int> countAllTasks() async {
    List<Map<String, dynamic>> result = await _db.rawQuery('SELECT COUNT(*) as count FROM $tableName');
    int count = Sqflite.firstIntValue(result) ?? 0;
    return count;
  }

  Future<int> countAllCompletedTasks() async {
    List<Map<String, dynamic>> result = await _db.rawQuery('SELECT COUNT(*) as count FROM $tableName WHERE isCompleted = 1');
    int count = Sqflite.firstIntValue(result) ?? 0;
    return count;
  }

  Future<void> updateTaskCompletion(int taskId, bool isCompleted) async {
    await _db.update(
      tableName,
      {'isCompleted': isCompleted ? 1 : 0},
      where: 'id = ?',
      whereArgs: [taskId],
    );
  }

  Future<double> calculatePercentageCompleted() async {
    List<Map<String, dynamic>> completedTasks = await _db.rawQuery('SELECT COUNT(*) as count FROM $tableName WHERE isCompleted = 1');
    int completedCount = Sqflite.firstIntValue(completedTasks) ?? 0;

    List<Map<String, dynamic>> allTasks = await _db.rawQuery('SELECT COUNT(*) as count FROM $tableName');
    int totalCount = Sqflite.firstIntValue(allTasks) ?? 0;

    // Avoid division by zero
    if (totalCount == 0) {
      return 0.0;
    }

    return (completedCount / totalCount) * 100;
  }
}
